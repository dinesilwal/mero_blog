from django.shortcuts import render


# Create your views here.

posts = [
    {
        'author': 'Dinesh',
        'title':'Blog Post 1',
        'content':'First post Content',
        'date_posted':'August 27,2020',

    },

    {
        'author': 'Nabin',
        'title': 'Blog Post 2',
        'content': 'Second post Content',
        'date_posted': 'August 28,2020',

    }
]



def home(request):
    context = {
        'posts': posts
    }
    return render(request, 'blog/home.html', context)


def about(request):
    return render(request, 'blog/about.html', {'title': 'about'})
